## About

This script provides a quick and easy means to download and install Drupal using a make file and perform various other post-installation setup in one step including:

* Create base theme from a Zen starter kit or other Zen-based base theme starter kit
* Setup a desired admin theme
* Optionally configure local host name, if desired/possible, in hosts file
* Remove and git-ignore Drupal txt files in the web root for security
* Modify .htaccess file to include mod deflate directives
* Optionally setup a drush alias for your staging server
* Disable user 1 and setup a secondary user with a developer role
* Optionally set permissions on the web root directory and it's contents to work properly on your system
* Optionally initialise git repository and perform initial commit
* Optionally push initial commit to remote repository

Some optional steps are prompted during installation. Others must be configured as to whether or not they are performed - see the configuration options for more details.

Note that this script contains some stuff specific to Kellett Communications, as that is where it was originally developed. It still needs some updating to make it more generic and generally reusable. Please feel free to fork this repository, make modifications as you see fit and submit a pull request to have it updated.

## Setup

You can clone the contents of this repository to any place you want on your computer. Then, either copy or symlink the drupal-install binary file into a folder that's in your path, such as /usr/local/bin or ~/bin, whatever suits you on your particular system. Ensure that the binary file itself has executable permissions.

Second, you must create a configuration file by copying the custom-config-example.cfg file into your home folder and renaming it to .drupal-install.cfg. Then, customise the configuration options in that file as appropriate for your needs. See documentation below for details on all the configuration options and how they are used. The example config file also contains documentation and examples of setting the various configuration options.

## Configuration options

The installer supports 2 types of configuration options:

* Runtime arguments that apply to the current installation, some of which will vary for each individual Drupal installation
* Common script configuration options that define how the script works or common options that should never change from one installation to the next

### Runtime arguments

Any of these options can be passed in via the command line. If any required options are not provided on the command line, you will be prompted to enter them during installation. Any of these options that you want to remain consistent for every installation can be defined in your configuration file.

-n, --dbname  
Required. The database name (invalid characters will automatically be replaced with underscores).

-u, --dbuser  
Required. The database user.

-p, --dbpass  
Optional. The database password.

-h, --dbhost  
Optional. The database hostname. The script sets this by default to '127.0.0.1' if not supplied or otherwise overridden by the config file.

-s, --sitename  
Required. The name of the website.

-a, --siteuser  
Required. The site admin username.

-f, --sitepass  
Required. The site admin password. Although it is a required argument, it can be left blank to have a random one generated. If that's what you want, don't bother passing it into the command line. Wait to be prompted and just hit enter at that point.

-e, --sitemail  
Required. The site and admin account email address.

-m, --makefile  
Required. The path to your makefile.

-r, --profile  
Required. The installation profile you want to use.

-H, --help  
Optional. Display usage instructions. If used, all other arguments will be ignored and only the usage instructions will be displayed.

### Configuration Options

`user1_email`  
String value. This is the email address to use for the user 1 account. Even though that account gets disabled, it still needs an email address associated with it. This is required for the installer to run.

`starterkit_theme_path`  
String value. This needs to be a path relative to the sites/all/themes directory without a preceding slash. It is required in order for the installer to run. Note that the installer also expects this starter kit theme to contain a file called STARTERKIT.info.txt that will be renamed according to the actual theme name you specify during installation and have it's .txt extension removed.

`set_permissions`  
Boolean value. Whether or not to set user group permissions on the web root directory and files after installation is complete. You may or may not need to do this on your local development environment, depending on what user and group you have Apache configured to run as. To use this feature properly, you must also set the next 2 options.

`usergroup`  
String value. If using the set_permissions option, this should be the user and/or group in the formatted expected by the chown command, eg `peter:_www` or `:apache`. This should be set only if needed to match your Apache configuration.

`permissions`  
String value. If using the set_permissions option, this should be the permissions to pass to chmod in standard posix format, e.g. `644`, or chmod notation such as `g+w`.

`modify_hosts`  
Boolean value. Whether or not to modify the system's hosts file to add the installation directory as a hostname pointing to 127.0.0.1.

`staging_hostname`  
String value, optional. This is used if you choose, when prompted, to setup a Drush alias for your staging server during installation. If provided, you don't need to enter your staging hostname every time. If you don't provide it, you'll be prompted to enter it.

`staging_path_prefix`  
String value, optional. This is also used if you choose, when prompted, to setup a Drush alias for your staging server during installation. Since web servers usually have all web root directories in the same parent folder, providing this saves you the time of having to enter the full path on every installation. You will only need to enter the path to the web root folder that comes after this parent folder prefix, if provided.

## Customise the Make File

The make file included with this script may not be ideally suited to your needs. You are expected to copy this and modify it, or provide your own, and then configure the path to the make file accordingly. However, the installer script itself, as noted above, still has a few hard-coded expectations for some stuff specific to Kellett Communications that you may not have access to. As such, please feel free to fork this repository and make changes to the script as you see fit to make it more generalised and reusable, then submit a pull request for review.
